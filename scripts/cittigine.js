function Cittigine(){
	//Global varibales
	this.dataScheme =
	{
		"achievement_scheme": {
			"properties": {
				"id": "number",
				"name": "string",
				"description": "string",
				"progress": "number",
				"earned": "boolean",
				"image": "string"
			}
		},
		"item_scheme": {
			"properties": {
				"id": "number",
				"name": "string",
				"description": "string",
				"quest_item": "boolean",
				"image": "string"
			}
		},
		"dialog_scheme": {
			"properties": {
				"id": "number",
				"title": "string",
				"text": "string",
				"options": "object",
				"image": "string"
			}
		},
		"option": {
			"properties": {
				"id": "numberde",
				"text": "string",
				"dialog_link": "number",
				"functions": "array",
				"enabled": "boolean"
			}
		},
		"functions":{
			"properties": {
				"command": "string",
				"arguments": "array"
			}
		}
	};
	this.gameData  = {};
	this.devMode = false;
	this.debugMode = false;
	this.currentDialog;
	this.saveData = {};
	//Engine functions
	//use the cittigine instance defined at the bottom of this file as the default cittigine instance value

	this.initializeEngine = function(){
		$("#interface").tabs();
		$('#creditsDialog').dialog();
		$('#creditsDialog').dialog("close");
		$('#aboutDialog').dialog();
		$('#aboutDialog').dialog("close");
		$('#menuCredits').on("click", function(){
			$('#creditsDialog').dialog("open");
			$("#menuHamburger svg").click();
		});
		$('#menuAbout').on("click", function(){
			$('#aboutDialog').dialog("open");
			$("#menuHamburger svg").click();
		});
		$("#inventoryWrapper")[0].innerHTML = "Your inventory is empty.";
		$("#menuHamburger svg").on('click', function(e){
			if($("#menuHamburger svg").hasClass('close')==true){
				$("#menuHamburger svg").removeClass('close');
				$("#menuHamburger svg").addClass('open');
				//$("#menu").css("display", "inline-block");
				$('#menu').show("blind", {}, 1000,function(){});
			} else if($("#menuHamburger svg").hasClass('open')==true) {
				$("#menuHamburger svg").removeClass('open');
				$("#menuHamburger svg").addClass('close');
				//$("#menu").css("display", "none");
				$('#menu').hide("blind", {}, 1000,function(){});
			} else{
				$("#menuHamburger svg").addClass('open');
				//$("#menu").css("display", "inline-block");
				$('#menu').show("blind", {}, 1000,function(){});
			}
		});
		$( "#menu" ).menu();
		$('#menu').hide("blind", {}, 0,function(){});
	}

	this.loadGame = function(gameName){
		var _this = this;
		$.getJSON(gameName + "/game_data.json", function(loadedData){
			_this.initializeGame(loadedData);
		});
	}

	this.initializeGame = function(loadedData){
		this.gameData=loadedData;
		this.debug("loadedData:")
		this.debug(loadedData);
		this.debug("gameData:")
		this.debug(this.gameData);
		this.validateGameData();
		this.loadDialog(1);
	}

	this.validateGameData = function(){
		console.log('Validateing achievements');
		for (var achievement in this.gameData.achievements){
			this.debug(achievement);
			for(var key in this.dataScheme.achievement_scheme.properties){
				if(this.gameData.achievements[achievement] == null)continue;
				this.debug(key + "->" + this.dataScheme.achievement_scheme.properties[key] + ": " + typeof(this.gameData.achievements[achievement][key]));
				this.debug(this.dataScheme.achievement_scheme.properties[key] == typeof(this.gameData.achievements[achievement][key]));
				if (this.dataScheme.achievement_scheme.properties[key] != typeof(this.gameData.achievements[achievement][key])){
					console.log(achievement+ " achievement does not conform to data scheme. It will be dropped so the game can load")
					this.alert(achievement+ " achievement does not conform to data scheme. It will be dropped so the game can load");
					this.gameData.achievements[achievement] = null;
				} else{
					this.gameData.achievements[achievement].currentProgress = 0;
					this.gameData.achievements[this.gameData.achievements[achievement].id] = this.gameData.achievements[achievement];
				}
			}
			if(this.gameData.achievements[achievement] != null) {
				this.buildAchievement(this.gameData.achievements[achievement].id);
			}
		}
		this.achievementTooltips();

		console.log('Validateing achievements complete');
		console.log('Validateing items');
		for (var item in this.gameData.items){
			this.debug(item);
			for(var key in this.dataScheme.item_scheme.properties){
				if(this.gameData.items[item] == null)continue;
				this.debug(key + "->" + this.dataScheme.item_scheme.properties[key] + ": " + typeof(this.gameData.items[item][key]));
				this.debug(this.dataScheme.item_scheme.properties[key] == typeof(this.gameData.items[item][key]));
				if (this.dataScheme.item_scheme.properties[key] != typeof(this.gameData.items[item][key])){
					console.log(item+ " item does not conform to data scheme. It will be dropped so the game can load")
					this.alert(item+ " item does not conform to data scheme. It will be dropped so the game can load");
					this.gameData.items[item] = null;
				}
			}
		}
		console.log('Validateing items complete');
	}

	this.loadDialog = function(treeID){
		var _this = this;
		console.log("loading dialog option" + treeID)
		if(treeID == this.gameData.game_params.start_dialog)this.inventoryClear();
		$.getJSON(_this.gameData.game_params.game_short_name + "/dialogs/" + treeID + ".json", function(loadedData){
			_this.debug(loadedData);
			_this.currentDialog = loadedData
			_this.parseDialog(loadedData);
		});
	}

	this.parseDialog = function(dialogString){
		this.debug(dialogString);
		$("#dialogTitle")[0].innerHTML = dialogString.title;
		$("#dialogText")[0].innerHTML = dialogString.text;
		var optionHTML = this.buildOptions(dialogString.options);
		$("#dialogOptions")[0].innerHTML = optionHTML;
		this.bindOptionFunctions(dialogString.options);
	}

	this.buildOptions = function(optionArray){
		console.log("Building options.")
		var returnHTML = "";
		var enabledClass;
		this.debug(optionArray);
		for (var cursor of optionArray){
			this.checkCondition(cursor);
			if(cursor.enabled == true)enabledClass = "enabled";
			else enabledClass = "disabled";
			returnHTML += '<li id="optionID' + cursor.id + '" class="option ' + enabledClass + '">' + cursor.text + '</li>';
		}
		return returnHTML;
	}

	this.bindOptionFunctions = function(optionArray){
		//rewrite this so that all functions are executed synconrously to avoice
		//race conditions and ensure propery condition checking before loading dialogs
		var _this = this;
		for (var cursor of optionArray){
			$("#optionID" + cursor.id).on("click", cursor.dialog_link, function(e){
				console.log("Loading dialog. ID:" + e.data);
				_this.loadDialog(e.data);
			});
			_this.debug(cursor.functions);
			var setNum = 0;
			for (var cursor2 of cursor.functions){
				var argSet = [];
				argSet[setNum] = [];
				_this.debug("command=" + cursor2.command);
				_this.debug("arg1=" + cursor2.arguments[0]);
				_this.debug("arg2=" + cursor2.arguments[1]);
				$("#optionID" + cursor.id).on("click", [cursor2.command, cursor2.arguments[0], cursor2.arguments[1]], function(e){
					console.log('Running post functions');
					console.log(e.data)
					_this.debug("command=" + e.data[0]);
					_this.debug("arg1=" + e.data[1]);
					_this.debug("arg2=" + e.data[2]);
					_this[e.data[0]](e.data[1],e.data[2]);
				});
				setNum ++;
			}
		}
	}

	this.buildAchievement = function(achievementID){
		console.log("Building achievement " + achievementID);
		var imageLocation = this.gameData.achievements[achievementID].image;
		var title = this.gameData.achievements[achievementID].name;
		var desc = this.gameData.achievements[achievementID].description;
		var id = this.gameData.achievements[achievementID].id;
		var gameFolder = this.gameData.game_params.game_short_name;
		var earned = '';
		if($('#achievementID' + id )[0] == undefined){
			var achievementWrapperHTML = '<div id="achievementID' + id + '" class="achievementWrapper"></div>';
			$("#achievementWrapper")[0].innerHTML += achievementWrapperHTML;
		}
		else{
			$('#achievementID' + id )[0].innerHTML = '';
		}
		if(this.gameData.achievements[achievementID].earned == true){earned = "earned";}

		var achievementHTML = '<img id="achievementID' + id + 'image" title = "' + title + '" desc = "' + desc + '" class="achievement ' + earned + '" src="' + gameFolder + imageLocation + '">';
		$('#achievementID' + id )[0].innerHTML = achievementHTML;
	}

	this.updateAchievement = function(achievementID, achievementProgress){
		console.log('Earn achievement ' + achievementID);
		if(this.gameData.achievements[achievementID].earned == true)return;
		this.gameData.achievements[achievementID].currentProgress += achievementProgress;
		if(this.gameData.achievements[achievementID].currentProgress == this.gameData.achievements[achievementID].progress){
			this.gameData.achievements[achievementID].earned = true;
			console.log('Earn achievement ' + this.gameData.achievements[achievementID].name + '(' + achievementID + ')');
			this.buildAchievement(achievementID);
			this.achievementTooltips();
		}
	}

	this.achievementTooltips = function(){
		$('.achievement.earned').tooltip({
			content:function(){
				var element = $( this );
				var tooltip = "<h3>" + element.attr("title") + "</h3><br>" + element.attr("desc");
				return tooltip;

			}
		});
		if (false)$('.achievement.earned').tooltip({
			content:function(){
				var element = $( this );
				var tooltip = "<h3>" + element.attr("title") + "</h3><br>" + element.attr("desc");
				return tooltip;

			}
		});
	}

	this.inventoryCreateEntry = function(item){
		console.log("Building item " + item);
		$('.item').tooltip('destroy');
		var imageLocation = this.gameData.items[item].image;
		var title = this.gameData.items[item].name;
		var desc = this.gameData.items[item].description;
		var id = this.gameData.items[item].id;
		var gameFolder = this.gameData.game_params.game_short_name;
		var quest = this.gameData.items[item].quest_item;
		var itemWrapperHTML = '<div id="itemID' + id + '" class="itemWrapper"></div>';
		$("#inventoryWrapper")[0].innerHTML += itemWrapperHTML;

		var itemHTML = '<img id="itemID' + id + 'image" title = "' + title + '" quest = "' + quest + '" desc = "' + desc + '" class="item" src="' + gameFolder + imageLocation + '">';
		$('#itemID' + id )[0].innerHTML = itemHTML;
	}

	this.inventoryTooltips = function(){
		$('.item').tooltip({
			content:function(){
				var element = $( this );
				var questItem = '';
				if(element.attr("quest") == "true"){
					questItem = " (Quest Item)";
				}
				var tooltip = "<h3>" + element.attr("title") + questItem + "</h3><br>" + element.attr("desc");
				return tooltip;

			}
		});
	}

	this.inventoryAdd = function(item, amount = 1){
		//$('.item').tooltip('destroy');
		console.log("Adding item " + item);
		this.debug($("#inventoryWrapper")[0].innerHTML);
		this.debug($("#inventoryWrapper")[0].innerHTML == 'Your inventory is empty.')
		if($("#inventoryWrapper")[0].innerHTML == 'Your inventory is empty.'){
			$("#inventoryWrapper")[0].innerHTML = '';
		}
		if($('#itemID' + this.gameData.items[item].id )[0] == undefined){
			this.inventoryCreateEntry(item);
		}
		if(this.gameData.inventory[item] == undefined){
			this.gameData.inventory[item] = amount
		}else{
			this.gameData.inventory[item] += amount;
		}
		this.inventoryTooltips();
	}

	this.inventoryRemove = function(item, amount = 1){
		$('.item').tooltip('destroy');
		console.log("Removing item " + item);
		if ((this.gameData.inventory[item] - amount) < 0)return false;
		else this.gameData.inventory[item] -= amount;
		if(this.gameData.inventory[item] == 0){
			$('#itemID' + this.gameData.items[item].id ).remove();
		}
		if($("#inventoryWrapper")[0].innerHTML == ''){
			$("#inventoryWrapper")[0].innerHTML = 'Your inventory is empty.';
		}
		this.inventoryTooltips();
	}

	this.inventoryClear = function(){
		$('.item').tooltip('destroy');
		this.gameData.inventory = {};
		$("#inventoryWrapper")[0].innerHTML = 'Your inventory is empty.';
	}

	this.checkCondition = function(option){
		this.debug(Object.keys(option.conditions).length === 0);
		if(Object.keys(option.conditions).length === 0) return;
		console.log(Object.keys(option.conditions));
		for (var condition in option.conditions){
			this.debug(condition);
			this.debug(this.gameData.game_state[condition]);
			this.debug(this.gameData.game_state[condition] == option.conditions[condition]);
			if(this.gameData.game_state[condition] == option.conditions[condition]){
				option.enabled = true;
			}else if (this.gameData.inventory[condition] >= option.conditions[condition]){
				option.enabled = true;
			}else{
				option.enabled = false;
			}
		}
	}

	this.setState = function(state, setTo){
		this.game_state[state] = setTo;
	}

	this.save = function(num){
		encodedSave = btoa(JSON.stringify(cittigine.gameData));
		saltString = '';
		seek = 0;
		console.log('salting')
		for(i = 1; i < num+3; i++){
			seek += i;
			console.log(seek);
			console.log(encodedSave[seek]);
			saltString += encodedSave[seek];
		}
		console.log(saltString);
	}

	this.alert = function(message){
		$('#alert')[0].innerHTML = message;
	}

	//function for setting debug mode, if no argument provided it toggles debugMode on and off.
	this.setDebugMode = function(state = ''){
		switch (state){
		case "":
			if (this.debugMode == false){
				this.debugMode = true;
				console.log("Debugging enabled");
			}else{
				this.debugMode = false;
				console.log("Debugging disabled");
			} 
			break;
		case "enable":
			this.debugMode = true;
			console.log("Debugging enabled");
			break;
		case "disable":
			this.debugMode = false;
			console.log("Debugging disabled");
			break;
		default:
			console.log("Invalid debug state passed");
		}
	}

	this.debug = function(message){
		if(this.debugMode == true){
			console.debug(message);
		}
	}

	this.setDevMode = function(state = ''){
		switch (state){
		case "":
			if (this.devMode == false){
				this.devMode = true;
				enableDev();
				this.setDebugMode('enable');
				console.log("Dev mode enabled");
			}else{
				this.devMode = false;
				disableDev();
				this.setDebugMode('disable');
				console.log("Dev mode disabled");
			} 
			break;
		case "enable":
			this.devMode = true;
			enableDev();
			this.setDebugMode('enable');
			console.log("Dev mode enabled");
			break;
		case "disable":
			this.devMode = false;
			disableDev();
			this.setDebugMode('disable');
			console.log("Dev mode disabled");
			break;
		default:
			console.log("Invalid dev mode state passed");
		}
	}

	function enableDev(){
		$('body').addClass('dev');
	}

	function disableDev(){
		$('body').removeClass('dev');
	}
}

//create new cittigine instance and  
let cittigine = new Cittigine;